import sys
if sys.version_info[0] < 3:
    from Tkinter import *    ## Python 2.x
else:
    from tkinter import *   ## Python 3.x
from math import *
window = Tk()

def main():
    window.wm_title('Main Screen')
    window.geometry("500x500")
    window.resizable(0, 0)
    Clear()
    

    t = ["Hey and welcome to this little program","Please select one of the assignments, you can always go back to this main screen"]
    x = 250
    y = 130
    for i in range(2):
        y = y+(25*i)
        text = t[i]
        Text(text,x,y)
    opgave0 = Button(text="Assignment 0", highlightthickness=0, activebackground="green", command=TextAnalyse)
    opgave0.place(x=100, y=200,anchor=CENTER)
    opgave1 = Button(text="Assignment 1", highlightthickness=0, activebackground="green", command=Volt)
    opgave1.place(x=200, y=200,anchor=CENTER)
    opgave2 = Button(text="Assignment 2", highlightthickness=0, activebackground="green", command=Circle)
    opgave2.place(x=300, y=200,anchor=CENTER)
    opgave4 = Button(text="Assignment 4", highlightthickness=0, activebackground="green", command=Tickets)
    opgave4.place(x=400, y=200,anchor=CENTER)
    opgave5 = Button(text="Assignment 5", highlightthickness=0, activebackground="green", command=Draw)
    opgave5.place(x=100, y=250,anchor=CENTER)
    opgave6 = Button(text="Assignment 6", highlightthickness=0, activebackground="green", command=Name)
    opgave6.place(x=200, y=250,anchor=CENTER)
    opgave7 = Button(text="Assignment 7", highlightthickness=0, activebackground="green", command=Deduction)
    opgave7.place(x=300, y=250,anchor=CENTER)
    opgave8 = Button(text="Assignment 8", highlightthickness=0, activebackground="green", command=CountDown)
    opgave8.place(x=400, y=250,anchor=CENTER)
    opgave9 = Button(text="Assignment 9", highlightthickness=0, activebackground="green", command=Tabel)
    opgave9.place(x=100, y=300,anchor=CENTER)
    opgave10 = Button(text="Assignment 10", highlightthickness=0, activebackground="green", command=Area)
    opgave10.place(x=200, y=300,anchor=CENTER)
    opgave11 = Button(text="Assignment 11", highlightthickness=0, activebackground="green", command=Sorter)
    opgave11.place(x=300, y=300,anchor=CENTER)
    opgave12 = Button(text="Assignment 12", highlightthickness=0, activebackground="green", command=Fibonacci)
    opgave12.place(x=400, y=300,anchor=CENTER)
    opgave13 = Button(text="Assignment 13", highlightthickness=0, activebackground="green", command=Parabel)
    opgave13.place(x=250, y=350,anchor=CENTER)

    window.mainloop()

class TextAnalyse():
    def __init__(self):
        Clear()
        window.wm_title('Assignment 0')
        goback = Button(text="Go Back", highlightthickness=0, activebackground="green", command=main)
        goback.place(x=50, y=50,anchor=CENTER)
        ShowImage = Button(window,text ='Show the safety protocols',command = self.button, height=5, width=20).pack()

    def button(self):
        novi = Toplevel()
        canvas = Canvas(novi, width = window.winfo_screenwidth(), height = window.winfo_screenwidth())
        canvas.pack(expand = YES, fill = BOTH)
        gif1 = PhotoImage(file = 'Opgave 0.png')
                                #image not visual
        canvas.create_image(0, 0, image = gif1, anchor = NW)
        #assigned the gif1 to the canvas object
        canvas.gif1 = gif1
class Volt():
    def __init__(self):
        Clear()
        window.wm_title('Assignment 1')
        goback = Button(text="Go Back", highlightthickness=0, activebackground="green", command=main)
        goback.place(x=50, y=50,anchor=CENTER)
        t = ["Voltage and effect calculator","What is the amp?", "What is the resitance?"]
        x = 200
        y = 150
        for l in range(3):
            y = y+25
            text = t[l]
            Text(text,x,y)
        self.i = Entry(window)
        self.R = Entry(window)
        b = Button(text="Calculate", highlightthickness=0, activebackground="green", command=self.calculate)
        self.i.place(x=320,y=200,anchor=CENTER)
        self.R.place(x=320,y=225,anchor=CENTER)
        b.place(x=250,y=250,anchor=CENTER)
        
    def calculate(self):
        self.i = int(self.i.get())
        R = int(self.R.get())
        self.U = self.i * R
        a = str(self.U)
        Text.answer(a,'V',250,270)
        t = "Want to know the effect?"
        Text(t,250,290)
        effect = Button(text="Yes", highlightthickness=0, activebackground="green", command=self.effect)
        effect.place(x=230, y=310,anchor=CENTER)
        goback = Button(text="no", highlightthickness=0, activebackground="green", command=main)
        goback.place(x=260, y=310,anchor=CENTER)

    def effect(self):
        p = int(self.U) * int(self.i)
        a = str(p)
        Text.answer(a,'W',250,340)
        goback = Button(text="Go Back", highlightthickness=0, activebackground="green", command=main)
        goback.place(x=260, y=360,anchor=CENTER)

class Circle():
    def __init__(self):
        Clear()
        window.wm_title('Assignment 2')
        goback = Button(text="Go Back", highlightthickness=0, activebackground="green", command=main)
        goback.place(x=50, y=50,anchor=CENTER)
        t = ["Circle calculator","What is the radius?"]
        x = 120
        y = 150
        for l in range(2):
            y = y+25
            text = t[l]
            Text(text,x,y)
        self.i = Entry(window)
        b = Button(text="Calculate", highlightthickness=0, activebackground="green", command=self.calculate)
        b.place(x=300,y=175,anchor=CENTER)
        self.i.place(x=320,y=200,anchor=CENTER)
        
    def calculate(self):
        i = int(self.i.get())
        O = 2*i*pi
        O = "The perimeter of the circle is: " + str(O)
        A = pi*i**2
        A = "The area of the cicle is: " + str(A)
        V = 4/3*pi*i**3
        V = "The volume is " + str(V)
        Text(O,250,250)
        Text(A,250,275)
        Text(V,250,300)
class Tickets():
    def __init__(self):
        Clear()
        window.wm_title('Assignment 4')
        goback = Button(text="Go Back", highlightthickness=0, activebackground="green", command=main)
        goback.place(x=50, y=50,anchor=CENTER)
        t = ["Ticket calculator","What is the age?"]
        x = 200
        y = 150
        for l in range(2):
            y = y+25
            text = t[l]
            Text(text,x,y)
        self.i = Entry(window)
        b = Button(text="Calculate", highlightthickness=0, activebackground="green", command=self.calculate)
        self.i.place(x=320,y=200,anchor=CENTER)
        b.place(x=250,y=230,anchor=CENTER)

    def calculate(self):
        i = int(self.i.get())
        if (i>=18 and i<60):
            a = 65
        elif (i<18):
            a = 35
        elif (i>=60):
            a = 30
        Text.answer(str(a),'kr',250,270)
        
class Draw():
    def __init__(self):
        Clear()
        window.wm_title('Assignment 5')
        goback = Button(text="Go Back", highlightthickness=0, activebackground="green", command=main)
        goback.place(x=50, y=50,anchor=CENTER)
        t = ["Draw a sqare"]
        x = 250
        y = 225
        for l in range(1):
            y = y+25
            text = t[l]
            Text(text,x,y)
        b = Button(text="Draw", highlightthickness=0, activebackground="green", command=self.draw)
        b.place(x=250,y=270,anchor=CENTER)

    def draw(self):
        line = Canvas(window)
        xStart = 192
        yStart = 232
        x = xStart+12*10
        y = yStart
        X = xStart
        Y = yStart
        for j in range(1):
            for k in range(1):
                for i in range(1):
                    for l in range(1):
                        line.create_line(X, Y, x, y)
                        line.pack(fill=BOTH, expand=1)
                        X = x
                        Y = y
                        y = yStart+12*10
                    line.create_line(X, Y, x, y)
                    line.pack(fill=BOTH, expand=1)
                    X = x
                    Y = y
                    x = x-12*10
                line.create_line(X, Y, x, y)
                line.pack(fill=BOTH, expand=1)
                X = x
                Y = y
                y = y-12*10
            line.create_line(X, Y, x, y)
            x = xStart+6*10
            y = yStart+6*10
            line.pack(fill=BOTH, expand=1)
            text = Label(window, text='Marc')
            text.place(x=x, y=y, anchor=CENTER)
            text = Label(window, text='Every thing is resized by a faktor of 10, so you can see it')
            text.place(x=250, y=150, anchor=CENTER)
            goback = Button(text="Go Back", highlightthickness=0, activebackground="green", command=main)
            goback.place(x=250, y=450,anchor=CENTER)
            
class Name():
    def __init__(self):
        Clear()
        window.wm_title('Assignment 6')
        goback = Button(text="Go Back", highlightthickness=0, activebackground="green", command=main)
        goback.place(x=50, y=50,anchor=CENTER)
        t = ["Guess my name"]
        x = 175
        y = 175
        self.g = 0
        for l in range(1):
            y = y+25
            text = t[l]
            Text(text,x,y)
        self.i = Entry(window)
        b = Button(text="Guess", highlightthickness=0, activebackground="green", command=self.calculate)
        self.i.place(x=320,y=200,anchor=CENTER)
        b.place(x=250,y=250,anchor=CENTER)

    def calculate(self):
        i = self.i.get()
        if (i=='marc' or i=='Marc'):
            t = 'Hurra, It only took '+str(self.g)+' tries'
            text = Label(window, text=t)
            text.place(x=250, y=300, anchor=CENTER)
            goback = Button(text="Go Back", highlightthickness=0, activebackground="green", command=main)
            goback.place(x=250, y=450,anchor=CENTER)
        else:
            self.g = self.g+1
            t = 'Nope, try again'
            text = Label(window, text=t)
            text.place(x=250, y=300, anchor=CENTER)
            
class Deduction():
    def __init__(self):
        Clear()
        window.wm_title('Assignment 7')
        goback = Button(text="Go Back", highlightthickness=0, activebackground="green", command=main)
        goback.place(x=50, y=50,anchor=CENTER)
        t = ["Deduction calculator","How many km have you driven?"]
        x = 170
        y = 150
        for l in range(2):
            y = y+25
            text = t[l]
            Text(text,x,y)
        self.i = Entry(window)
        b = Button(text="Calculate", highlightthickness=0, activebackground="green", command=self.calculate)
        self.i.place(x=320,y=200,anchor=CENTER)
        b.place(x=250,y=230,anchor=CENTER)

    def calculate(self):
        i = int(self.i.get())
        if (i>=25 and i<=100):
            a = 154*i
        elif (i<24):
            a = 0
        elif (i>=100):
            a = 77*i
        Text.answer(str(a),'øre',250,270)
        
class CountDown():
    def __init__(self):
        Clear()
        window.wm_title('Assignment 8')
        goback = Button(text="Go Back", highlightthickness=0, activebackground="green", command=main)
        goback.place(x=50, y=50,anchor=CENTER)
        t = "Count down"
        x = 150
        y = 150
        Text(t,x,y)
        self.b = Button(text="Count Down", highlightthickness=0, activebackground="green", command=self.calculate)
        self.b.place(x=250,y=250,anchor=CENTER)

    def calculate(self):
        x = 250
        y = 50
        self.b.destroy()
        for i in range(20):
            t = i+1
            Text(t,x,y)
            y = y+20
        goback = Button(text="Go Back", highlightthickness=0, activebackground="green", command=main)
        goback.place(x=250, y=480,anchor=CENTER)
        
class Tabel():
    def __init__(self):
        Clear()
        window.wm_title('Assignment 9')
        goback = Button(text="Go Back", highlightthickness=0, activebackground="green", command=main)
        goback.place(x=50, y=50,anchor=CENTER)
        t = ["Tabel calculator","Which tabel wants you to calculate?"]
        x = 140
        y = 150
        for l in range(2):
            y = y+25
            text = t[l]
            Text(text,x,y)
        self.i = Entry(window)
        b = Button(text="Calculate", highlightthickness=0, activebackground="green", command=self.calculate)
        self.i.place(x=320,y=200,anchor=CENTER)
        b.place(x=250,y=230,anchor=CENTER)

    def calculate(self):
        n = int(self.i.get())
        l = []
        for i in range(10):
            N = n*(i+1)
            l.append(N)
        Text(str(l),250,270)
class Area():
    def __init__(self):
        Clear()
        window.wm_title('Assignment 10')
        goback = Button(text="Go Back", highlightthickness=0, activebackground="green", command=main)
        goback.place(x=50, y=50,anchor=CENTER)
        t = ["Area calculator","What figur area want you to calculate?"]
        x = 250
        y = 150
        for l in range(2):
            y = y+25
            text = t[l]
            Text(text,x,y)
        triangle = Button(text="Triangle", highlightthickness=0, activebackground="green", command=self.triangle)
        rectangel = Button(text="Rectangel", highlightthickness=0, activebackground="green", command=self.rectangel)
        circle = Button(text="Circle", highlightthickness=0, activebackground="green", command=self.circle)
        triangle.place(x=250,y=230,anchor=CENTER)
        rectangel.place(x=250,y=260,anchor=CENTER)
        circle.place(x=250,y=290,anchor=CENTER)

    def triangle(self):
        Clear()
        goback = Button(text="Go Back", highlightthickness=0, activebackground="green", command=Area)
        goback.place(x=50, y=50,anchor=CENTER)
        t = ["Triangle area calculator","What is the height?","What is the width of the groundline?","Which measurement unit is used?"]
        x = 160
        y = 175
        for l in range(4):
            y = y+25
            text = t[l]
            Text(text,x,y)
        self.h = Entry(window)
        self.g = Entry(window)
        self.u = Entry(window)
        self.h.place(x=330,y=225,anchor=CENTER)
        self.g.place(x=330,y=250,anchor=CENTER)
        self.u.place(x=330,y=275,anchor=CENTER)
        b = Button(text="Calculate", highlightthickness=0, activebackground="green", command=self.calculateTriangle)
        b.place(x=250,y=300,anchor=CENTER)
    def rectangel(self):
        Clear()
        goback = Button(text="Go Back", highlightthickness=0, activebackground="green", command=Area)
        goback.place(x=50, y=50,anchor=CENTER)
        t = ["Rectangel area calculator","What is the height?","What is the width?","Which measurement unit is used?"]
        x = 160
        y = 175
        for l in range(4):
            y = y+25
            text = t[l]
            Text(text,x,y)
        self.a = Entry(window)
        self.b = Entry(window)
        self.u = Entry(window)
        self.a.place(x=330,y=225,anchor=CENTER)
        self.b.place(x=330,y=250,anchor=CENTER)
        self.u.place(x=330,y=275,anchor=CENTER)
        b = Button(text="Calculate", highlightthickness=0, activebackground="green", command=self.calculateRectangel)
        b.place(x=250,y=300,anchor=CENTER)
    def circle(self):
        Clear()
        goback = Button(text="Go Back", highlightthickness=0, activebackground="green", command=Area)
        goback.place(x=50, y=50,anchor=CENTER)
        t = ["Circle area calculator","What is the radius?","Which measurement unit is used?"]
        x = 160
        y = 175
        for l in range(3):
            y = y+25
            text = t[l]
            Text(text,x,y)
        self.r = Entry(window)
        self.u = Entry(window)
        self.r.place(x=330,y=225,anchor=CENTER)
        self.u.place(x=330,y=250,anchor=CENTER)
        b = Button(text="Calculate", highlightthickness=0, activebackground="green", command=self.calculateCircle)
        b.place(x=250,y=275,anchor=CENTER)
    def calculateTriangle(self):
        u = str(self.u.get())
        u = u+'^2'
        h = int(self.h.get())
        g = int(self.g.get())
        A = 0.5*h*g
        Text.answer(str(A),u,250,350)
        goback = Button(text="Go Back To Main Screen", highlightthickness=0, activebackground="green", command=main)
        goback.place(x=250, y=400,anchor=CENTER)
    def calculateRectangel(self):
        u = str(self.u.get())
        u = u+'^2'
        a = int(self.a.get())
        b = int(self.b.get())
        A = a*b
        Text.answer(str(A),u,250,350)
        goback = Button(text="Go Back To Main Screen", highlightthickness=0, activebackground="green", command=main)
        goback.place(x=250, y=400,anchor=CENTER)
    def calculateCircle(self):
        u = str(self.u.get())
        u = u+'^2'
        r = int(self.r.get())
        A = pi*r**2
        Text.answer(str(A),u,250,350)
        goback = Button(text="Go Back To Main Screen", highlightthickness=0, activebackground="green", command=main)
        goback.place(x=250, y=400,anchor=CENTER)
class Sorter():
    def __init__(self):
        Clear()
        window.wm_title('Assignment 11')
        goback = Button(text="Go Back", highlightthickness=0, activebackground="green", command=main)
        goback.place(x=50, y=50,anchor=CENTER)
        t = ["Number sorter","Number 1","Number 2","Number 3","Number 4"]
        x = 175
        y = 150
        for l in range(5):
            y = y+25
            text = t[l]
            Text(text,x,y)
        self.b1 = Entry(window)
        self.b2 = Entry(window)
        self.b3 = Entry(window)
        self.b4 = Entry(window)
        self.b1.place(x=275,y=200,anchor=CENTER)
        self.b2.place(x=275,y=225,anchor=CENTER)
        self.b3.place(x=275,y=250,anchor=CENTER)
        self.b4.place(x=275,y=275,anchor=CENTER)
        b = Button(text="Sort it", highlightthickness=0, activebackground="green", command=self.sort)
        b.place(x=250,y=300,anchor=CENTER)
    def sort(self):
        l = []
        b1 = int(self.b1.get())
        b2 = int(self.b2.get())
        b3 = int(self.b3.get())
        b4 = int(self.b4.get())
        l.append(b1)
        l.append(b2)
        l.append(b3)
        l.append(b4)
        a = sorted(l)
        Text.answer(str(a),'',250,350)
        goback = Button(text="Go Back To Main Screen", highlightthickness=0, activebackground="green", command=main)
        goback.place(x=250, y=400,anchor=CENTER)
class Fibonacci():
    def __init__(self):
        Clear()
        window.wm_title('Assignment 12')
        goback = Button(text="Go Back", highlightthickness=0, activebackground="green", command=main)
        goback.place(x=50, y=50,anchor=CENTER)
        t = ["Fibonacci numbers"]
        x = 170
        y = 150
        for l in range(1):
            y = y+25
            text = t[l]
            Text(text,x,y)
        b = Button(text="Show", highlightthickness=0, activebackground="green", command=self.calculate)
        b.place(x=250,y=275,anchor=CENTER)

    def calculate(self):
        Clear()
        x = 250
        y = 100
        a = 0
        b = 1
        l = []
        for i in range(15):
            c = a + b
            a = b 
            b = c
            l.append(str(b))
        t=sorted(l,reverse = True)
        Text(t,x,y)
        y = y + 20
        goback = Button(text="Go Back", highlightthickness=0, activebackground="green", command=main)
        goback.place(x=250, y=450,anchor=CENTER)

class Parabel():
    def __init__(self):
        Clear()
        window.wm_title('Assignment 13')
        goback = Button(text="Go Back", highlightthickness=0, activebackground="green", command=main)
        goback.place(x=50, y=50,anchor=CENTER)
        t = ["Calculator for parabola vertex and roots","a", "b","c"]
        x = 170
        y = 150
        for l in range(4):
            y = y+25
            text = t[l]
            Text(text,x,y)
        self.a = Entry(window)
        self.b = Entry(window)
        self.c = Entry(window)
        b = Button(text="Calculate", highlightthickness=0, activebackground="green", command=self.calculate)
        self.a.place(x=320,y=200,anchor=CENTER)
        self.b.place(x=320,y=225,anchor=CENTER)
        self.c.place(x=320,y=250,anchor=CENTER)
        b.place(x=250,y=275,anchor=CENTER)

    def calculate(self):
        b = int(self.b.get())
        a = int(self.a.get())
        c = int(self.c.get())
        D = b**2-4*a*c
        t = 0
        Ty = -D/(4*a)
        Tx = -b/(2*a)
        X1 = (-b+sqrt(D))/(2*a)
        X2 = (-b-sqrt(D))/(2*a)
        if (X1!=0):
            t = t+1
        if (X2!=0):
            t = t+1
        if (t!=0):
            t = "There are " + str(t) + "roots"
            text = Label(window, text=t)
            text.place(x=250, y=300, anchor=CENTER)
            t = "The parabola vertex is at (" + str(Tx) + ";" + str(Ty) + ")"
            text = Label(window, text=t)
            text.place(x=250, y=325, anchor=CENTER)
            '''
            line = Canvas(window)
            xStart = 100
            yStart = 400
            X = xStart
            Y = yStart
            x = xStart
            for j in range(100):
                for i in range(1):
                    x = (j+1) * xStart
                    y = a * x**2+b*x+c
                    line.create_line(X, Y, x, y)
                    line.pack(fill=BOTH, expand=1)
                    X = x
                Y = y
            '''
        else:
            t = "There are 0 roots"
            text = Label(window, text=t)
            text.place(x=250, y=300, anchor=CENTER)
            t = "The parabola vertex is at (" + str(Tx) + ";" + str(Ty) + ")"
        
class Clear():
    def __init__(self):
        l = window.winfo_children()
        for i in range(0,len(l)):
            l[i].destroy()
        
class Text():
    def __init__(self,t,x,y):
        text = Label(window, text=t)
        text.place(x=x, y=y, anchor=CENTER)
    def answer(a,U,x,y):
        answer = Label(window, text='The answer is: ' + a + U)
        answer.place(x=x, y=y, anchor=CENTER)
        
if __name__ == '__main__':
    main()
